package ru.t1.nkiryukhin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.nkiryukhin.tm.model.Session;

import java.util.List;

@Repository
@Scope("prototype")
public interface SessionRepository extends AbstractRepository<Session> {

    @NotNull
    List<Session> findByUserId(@NotNull final String userId);

}
