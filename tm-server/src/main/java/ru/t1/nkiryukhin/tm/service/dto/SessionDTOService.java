package ru.t1.nkiryukhin.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.nkiryukhin.tm.api.service.dto.ISessionDTOService;
import ru.t1.nkiryukhin.tm.dto.model.SessionDTO;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;
import ru.t1.nkiryukhin.tm.exception.field.IdEmptyException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;
import ru.t1.nkiryukhin.tm.repository.dto.SessionDTORepository;

import java.util.List;
import java.util.Optional;

@Service
public final class SessionDTOService extends AbstractDTOService<SessionDTO> implements ISessionDTOService {

    @NotNull
    @Autowired
    private SessionDTORepository repository;

    @NotNull
    @Override
    protected SessionDTORepository getRepository() {
        return repository;
    }

    @Override
    public boolean existsById(@Nullable final String id) throws AbstractFieldException {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findByUserId(userId);
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@Nullable final String id) throws AbstractFieldException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Optional<SessionDTO> result = repository.findById(id);
        return result.orElse(null);
    }

    @Override
    public int getSize() {
        return (int) repository.count();
    }

    @Override
    @Transactional
    public void remove(final List<SessionDTO> sessions) {
        for (final SessionDTO session : sessions) {
            removeOne(session);
        }
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) throws AbstractFieldException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        SessionDTO session = findOneById(id);
        removeOne(session);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        List<SessionDTO> sessions = findAll(userId);
        if (sessions == null || sessions.isEmpty()) return;
        remove(sessions);
    }

}
