package ru.t1.nkiryukhin.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.nkiryukhin.tm.api.service.IPropertyService;


@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @Value("#{environment['application.version']}")
    private String applicationVersion;

    @Value("#{environment['database.driver']}")
    private String databaseDriver;

    @Value("#{environment['database.name']}")
    private String databaseName;

    @Value("#{environment['database.username']}")
    private String databaseUser;

    @Value("#{environment['database.password']}")
    private String databasePassword;

    @Value("#{environment['database.dialect']}")
    private String databaseDialect;

    @Value("#{environment['database.show_sql']}")
    private String databaseShowSQL;

    @Value("#{environment['database.hbm2ddl_auto']}")
    private String databaseHbm2ddlAuto;

    @Value("#{environment['buildNumber']}")
    private String buildNumber;

    @Value("#{environment['author.name']}")
    private String authorName;

    @Value("#{environment['author.email']}")
    private String authorEmail;

    @Value("#{environment['developer']}")
    private String developer;

    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;

    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @Value("#{environment['server.host']}")
    private String serverHost;

    @Value("#{environment['server.port']}")
    private String serverPort;

    @Value("#{environment['session.key']}")
    private String sessionKey;

    @Value("#{environment['session.timeout']}")
    private Integer sessionTimeout;

    @Value("#{environment['database.server.url']}")
    private String databaseServerUrl;

    @Value("#{environment['dba.passphrase']}")
    private String dbaPassphrase;

    @Value("#{environment['database.cache.use_second_level_cache']}")
    private String databaseUseSecondLevelCache;

    @Value("#{environment['database.cache.use_query_cache']}")
    private String databaseUseQueryCache;

    @Value("#{environment['database.cache.use_minimal_puts']}")
    private String databaseUseMinimalPuts;

    @Value("#{environment['database.cache.region_prefix']}")
    private String databaseCacheRegionPrefix;

    @Value("#{environment['database.cache.provider_configuration_file_resource_path']}")
    private String databaseCacheProviderConfigurationFile;

    @Value("#{environment['database.cache.region.factory_class']}")
    private String databaseCacheRegionFactoryClass;

    @Value("#{environment['jms.broker.address']}")
    private String brokerAddress;

    @NotNull
    @Override
    public String getDatabaseUrl() {
        return databaseServerUrl + databaseName;
    }

}