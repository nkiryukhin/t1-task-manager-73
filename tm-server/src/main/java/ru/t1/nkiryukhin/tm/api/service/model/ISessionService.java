package ru.t1.nkiryukhin.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;
import ru.t1.nkiryukhin.tm.model.Session;

import java.util.List;

public interface ISessionService extends IService<Session> {

    void clear(@Nullable String userId) throws UserIdEmptyException;

    boolean existsById(@Nullable String id) throws AbstractFieldException;

    List<Session> findAll(@Nullable String userId) throws UserIdEmptyException;

    Session findOneById(@Nullable String id) throws AbstractFieldException;

    int getSize();

    void remove(List<Session> sessions);

    void removeById(@Nullable String id) throws AbstractFieldException;

}