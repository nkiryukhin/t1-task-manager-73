package ru.t1.nkiryukhin.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.nkiryukhin.tm.api.service.model.ISessionService;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;
import ru.t1.nkiryukhin.tm.exception.field.IdEmptyException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;
import ru.t1.nkiryukhin.tm.model.Session;
import ru.t1.nkiryukhin.tm.repository.model.SessionRepository;

import java.util.List;

@Service
public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    @Autowired
    private SessionRepository repository;

    @NotNull
    @Override
    protected SessionRepository getRepository() {
        return repository;
    }

    @Override
    public boolean existsById(@Nullable final String id) throws AbstractFieldException {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public List<Session> findAll(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findByUserId(userId);
    }

    @Nullable
    @Override
    public Session findOneById(@Nullable final String id) throws AbstractFieldException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id).orElse(null);
    }

    @Override
    public int getSize() {
        return (int) repository.count();
    }

    @Override
    @Transactional
    public void remove(final List<Session> sessions) {
        for (final Session session : sessions) {
            removeOne(session);
        }
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) throws AbstractFieldException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        Session session = findOneById(id);
        removeOne(session);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        List<Session> sessions = findAll(userId);
        if (sessions == null || sessions.isEmpty()) return;
        remove(sessions);
    }

}
