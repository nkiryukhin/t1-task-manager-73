package ru.t1.nkiryukhin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import ru.t1.nkiryukhin.tm.dto.model.TaskDTO;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface TaskDTORepository extends AbstractUserOwnedDTORepository<TaskDTO> {

    long countByUserId(@NotNull String userId);

    @Nullable
    List<TaskDTO> findByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    void deleteByUserId(@NotNull String userId);

    @NotNull
    List<TaskDTO> findByUserId(@NotNull String userId);

    @NotNull
    List<TaskDTO> findByUserId(@NotNull String userId, @NotNull Sort sort);

    @NotNull
    Optional<TaskDTO> findByUserIdAndId(@NotNull String userId, @NotNull String id);

}
