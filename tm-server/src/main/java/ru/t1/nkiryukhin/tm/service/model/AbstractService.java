package ru.t1.nkiryukhin.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.nkiryukhin.tm.api.service.model.IService;
import ru.t1.nkiryukhin.tm.comparator.CreatedComparator;
import ru.t1.nkiryukhin.tm.comparator.NameComparator;
import ru.t1.nkiryukhin.tm.model.AbstractModel;
import ru.t1.nkiryukhin.tm.repository.model.AbstractRepository;

import java.util.Comparator;

@Service
public abstract class AbstractService<M extends AbstractModel> implements IService<M> {

    @NotNull
    protected abstract AbstractRepository<M> getRepository();

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        if (comparator == NameComparator.INSTANCE) return "name";
        else return "status";
    }

    @Nullable
    @Override
    @Transactional
    public M add(@Nullable final M model) {
        if (model == null) return null;
        getRepository().save(model);
        return model;
    }

    @Override
    @Transactional
    public void removeOne(@Nullable final M model) {
        if (model == null) return;
        getRepository().delete(model);
    }

    @Override
    @Transactional
    public void update(@Nullable final M model) {
        if (model == null) return;
        getRepository().save(model);
    }

}
