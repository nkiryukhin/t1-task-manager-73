package ru.t1.nkiryukhin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.nkiryukhin.tm.api.endpoint.ISystemEndpoint;
import ru.t1.nkiryukhin.tm.enumerated.Role;
import ru.t1.nkiryukhin.tm.listener.AbstractListener;

@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @NotNull
    @Autowired
    protected AbstractListener[] listeners;

    @NotNull
    @Autowired
    protected ISystemEndpoint systemEndpoint;

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
