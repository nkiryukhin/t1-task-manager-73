package ru.t1.nkiryukhin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    IAuthEndpoint getAuthEndpoint();

    @NotNull
    IAdminEndpoint getAdminEndpoint();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISystemEndpoint getSystemEndpoint();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    ITokenService getTokenService();

    @NotNull
    IUserEndpoint getUserEndpoint();

}