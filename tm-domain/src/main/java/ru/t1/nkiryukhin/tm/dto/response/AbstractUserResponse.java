package ru.t1.nkiryukhin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.dto.model.UserDTO;

@Getter
@Setter
@NoArgsConstructor
public class AbstractUserResponse extends AbstractResponse {

    @Nullable
    private UserDTO user;

    public AbstractUserResponse(@Nullable UserDTO user) {
        this.user = user;
    }

}
