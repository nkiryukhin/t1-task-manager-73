package ru.t1.nkiryukhin.tm.dto.response;

import lombok.NoArgsConstructor;
import ru.t1.nkiryukhin.tm.dto.model.UserDTO;

@NoArgsConstructor
public final class UserRegistryResponse extends AbstractUserResponse {

    public UserRegistryResponse(UserDTO user) {
        super(user);
    }

}