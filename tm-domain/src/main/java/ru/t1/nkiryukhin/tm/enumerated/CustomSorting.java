package ru.t1.nkiryukhin.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.comparator.CreatedComparator;
import ru.t1.nkiryukhin.tm.comparator.NameComparator;
import ru.t1.nkiryukhin.tm.comparator.StatusComparator;

import java.util.Comparator;

public enum CustomSorting {

    BY_NAME("Sort by Name", NameComparator.INSTANCE),
    BY_STATUS("Sort by Status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by Created", CreatedComparator.INSTANCE);

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator<?> comparator;

    CustomSorting(@NotNull final String displayName, @NotNull final Comparator<?> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @Nullable
    public static CustomSorting toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final CustomSorting sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @NotNull
    @SuppressWarnings("rawtypes")
    public Comparator getComparator() {
        return comparator;
    }

}
