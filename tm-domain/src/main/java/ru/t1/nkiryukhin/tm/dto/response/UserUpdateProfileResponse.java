package ru.t1.nkiryukhin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.nkiryukhin.tm.dto.model.UserDTO;

@Getter
@Setter
@NoArgsConstructor
public final class UserUpdateProfileResponse extends AbstractUserResponse {

    public UserUpdateProfileResponse(UserDTO user) {
        super(user);
    }

}