package ru.t1.nkiryukhin.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.nkiryukhin.tm.model.User;

public interface UserRepository extends JpaRepository<User, String> {

    User findByLogin(String login);

}