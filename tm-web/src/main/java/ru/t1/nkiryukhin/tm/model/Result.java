package ru.t1.nkiryukhin.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class Result {

    @NotNull
    private Boolean success = true;

    @Nullable
    private String message = "";

    public Result(@NotNull final Boolean success) {
        this.success = success;
    }

    public Result(@NotNull final Exception e) {
        success = false;
        message = e.getMessage();
    }

}

