package ru.t1.nkiryukhin.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.nkiryukhin.tm.model.CustomUser;
import ru.t1.nkiryukhin.tm.repository.UserRepository;

@Controller
public class UsersController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/users")
    @PreAuthorize("hasRole('ADMIN')")
    public ModelAndView index(@AuthenticationPrincipal final CustomUser user) {
        return new ModelAndView("user-list", "users", userRepository.findAll());
    }

}