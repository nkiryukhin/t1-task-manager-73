package ru.t1.nkiryukhin.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.nkiryukhin.tm.dto.UserDTO;

import java.util.List;

public interface UserDTORepository extends JpaRepository<UserDTO, String> {

    List<UserDTO> findAllByLoginContainingIgnoreCase(String substring);

    UserDTO findByLogin(String login);

    @Override
    @Transactional
    void delete(UserDTO userDTO);

    @Override
    @Transactional
    void deleteById(String id);

}