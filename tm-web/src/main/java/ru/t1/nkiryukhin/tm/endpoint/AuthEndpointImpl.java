package ru.t1.nkiryukhin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.t1.nkiryukhin.tm.api.AuthEndpoint;
import ru.t1.nkiryukhin.tm.dto.UserDTO;
import ru.t1.nkiryukhin.tm.model.Result;
import ru.t1.nkiryukhin.tm.repository.UserDTORepository;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@RestController
@RequestMapping("/api/auth")
@WebService(endpointInterface = "ru.t1.nkiryukhin.tm.api.AuthEndpoint")
public class AuthEndpointImpl implements AuthEndpoint {

    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDTORepository userRepository;

    @Override
    @WebMethod
    @PostMapping("/login")
    public Result login(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password
    ) {
        try {
            @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            @NotNull final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new Result(authentication.isAuthenticated());
        } catch (@NotNull final Exception e) {
            return new Result(e);
        }
    }

    @Override
    @WebMethod
    @GetMapping("/profile")
    public UserDTO profile() {
        @NotNull final SecurityContext securityContext = SecurityContextHolder.getContext();
        @NotNull final Authentication authentication = securityContext.getAuthentication();
        @NotNull final String username = authentication.getName();
        return userRepository.findByLogin(username);
    }

    @Override
    @WebMethod
    @PostMapping("/logout")
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

}