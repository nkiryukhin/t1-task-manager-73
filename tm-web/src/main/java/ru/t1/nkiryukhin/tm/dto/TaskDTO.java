package ru.t1.nkiryukhin.tm.dto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.nkiryukhin.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "web_task")
public final class TaskDTO {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    private String name;

    @NotNull
    private String description = "";

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date created = new Date();

    @Nullable
    @Column(name = "project_id")
    private String projectId;

    @Nullable
    @Column(name = "user_id")
    private String userId;

    public TaskDTO(@NotNull final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name + " : " + description + " : " + status.getDisplayName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskDTO task = (TaskDTO) o;
        return created.equals(task.created)
                && name.equals(task.name)
                && Objects.equals(projectId, task.projectId)
                && status == task.status;
    }

}