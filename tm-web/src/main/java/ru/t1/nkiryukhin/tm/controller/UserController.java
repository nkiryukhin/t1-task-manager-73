package ru.t1.nkiryukhin.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.nkiryukhin.tm.enumerated.RoleType;
import ru.t1.nkiryukhin.tm.model.CustomUser;
import ru.t1.nkiryukhin.tm.model.Role;
import ru.t1.nkiryukhin.tm.model.User;
import ru.t1.nkiryukhin.tm.repository.UserRepository;

import java.util.Collections;

@Controller
@PreAuthorize("hasRole('ADMIN')")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping("/user/create")
    public String create(
            @AuthenticationPrincipal final CustomUser customUser
    ) {
        final User user = new User("New User " + System.currentTimeMillis());
        final Role role = new Role();
        role.setUser(user);
        role.setRoleType(RoleType.USUAL);
        user.setRoles(Collections.singletonList(role));
        userRepository.save(user);
        return "redirect:/users";
    }

    @Transactional
    @GetMapping("/user/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) {
        userRepository.deleteById(id);
        return "redirect:/users";
    }

    @GetMapping("/user/edit/{id}")
    public ModelAndView edit(
            @PathVariable("id") String id
    ) {
        final User user = userRepository.findById(id).orElse(null);
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("user-edit");
        modelAndView.addObject("user", user);
        modelAndView.addObject("roles", RoleType.values());
        return modelAndView;
    }

    @PostMapping("/user/edit/{id}")
    public String edit(
            @ModelAttribute("user") User user,
            BindingResult result
    ) {
        final String password = user.getPasswordHash();
        final String passwordHash = passwordEncoder.encode(password);
        user.setPasswordHash(passwordHash);
        userRepository.save(user);
        return "redirect:/users";
    }

}