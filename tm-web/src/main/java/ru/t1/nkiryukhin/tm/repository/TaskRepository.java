package ru.t1.nkiryukhin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.nkiryukhin.tm.model.Project;
import ru.t1.nkiryukhin.tm.model.Task;

import java.util.Collection;


@Repository
public interface TaskRepository extends JpaRepository<Task, String> {

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @NotNull
    Collection<Task> findAllByUserId(@NotNull final String userId);

}