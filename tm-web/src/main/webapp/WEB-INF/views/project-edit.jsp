<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>

<h1>PROJECT EDIT</h1>
<form:form action="/project/edit/${project.id}" method="POST" modelAttribute="project">
<form:input type="hidden" path="id"/>

<p>
    <div>NAME:</div>
    <div><form:input type="text" path="name"/></div>
</p>

<p>
    <div>DESCRIPTION:</div>
    <div><form:input type="text" path="description"/></div>
</p>

<p>
    <div>STATUS:</div>
    <div>
        <form:select path="status">
            <form:option value="${null}" label="--- // ---"/>
            <form:options items="${statuses}" itemLabel="displayName"/>
        </form:select>
    </div>
</p>

<p>
    <div>CREATED:</div>
    <div>
        <form:input type="date" path="created"/>
    </div>
</p>

<button type="submit">SAVE PROJECT</button>

</form:form>

<jsp:include page="../include/_footer.jsp"/>