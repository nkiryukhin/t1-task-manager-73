<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>

<h1>USER EDIT</h1>
<form:form action="/user/edit/${user.id}" method="POST" modelAttribute="user">
<form:input type="hidden" path="id"/>

<p>
    <div>LOGIN:</div>
    <div><form:input type="text" path="login"/></div>
</p>

<p>
    <div>PASSWORD:</div>
    <div><form:input type="text" path="passwordHash"/></div>
</p>

<button type="submit">SAVE USER</button>

</form:form>

<jsp:include page="../include/_footer.jsp"/>