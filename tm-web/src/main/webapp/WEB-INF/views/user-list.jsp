<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>
<h1>USER LIST</h1>

<table width="50%" cellpadding="10" border="1" style="border-collapse: collapse">
    <tr>
        <th width="100" nowrap="nowrap">ID</th>
        <th width="100" nowrap="nowrap" align="left">NAME</th>
        <th width="100" nowrap="nowrap" align="center">EDIT</th>
        <th width="100" nowrap="nowrap" align="center">DELETE</th>
    </tr>

    <c:forEach var="user" items="${users}">
        <tr>
            <td>
                <c:out value="${user.id}"/>
            </td>
            <td>
                <c:out value="${user.login}"/>
            </td>
            <td align="center">
                <a href="/user/edit/${user.id}"/>EDIT</a>
            </td>
            <td align="center">
                <a href="/user/delete/${user.id}"/>DELETE</a>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="/user/create" style="margin-top: 20px;">
    <button>CREATE USER</button>
</form>

<jsp:include page="../include/_footer.jsp"/>