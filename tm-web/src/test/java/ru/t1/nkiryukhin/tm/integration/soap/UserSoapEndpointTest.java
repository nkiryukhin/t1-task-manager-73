package ru.t1.nkiryukhin.tm.integration.soap;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.springframework.data.util.CastUtils;
import org.springframework.http.HttpHeaders;
import ru.t1.nkiryukhin.tm.api.AuthEndpoint;
import ru.t1.nkiryukhin.tm.api.UserEndpoint;
import ru.t1.nkiryukhin.tm.client.soap.AuthSoapEndpointClient;
import ru.t1.nkiryukhin.tm.client.soap.UserSoapEndpointClient;
import ru.t1.nkiryukhin.tm.dto.UserDTO;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.*;

public class UserSoapEndpointTest {

    @NotNull
    private static AuthEndpoint AUTH_ENDPOINT;

    @NotNull
    private static UserEndpoint USER_ENDPOINT;

    @NotNull
    private static final String URL = "http://localhost:8080";

    @NotNull
    private final UserDTO user1 = new UserDTO("Test User 1");

    @NotNull
    private final UserDTO user2 = new UserDTO("Test User 2");

    private static long initCountUser = 0;

    @BeforeClass
    public static void beforeClass() throws Exception {
        AUTH_ENDPOINT = AuthSoapEndpointClient.getInstance(URL);
        Assert.assertTrue(AUTH_ENDPOINT.login("admin", "admin").getSuccess());
        USER_ENDPOINT = UserSoapEndpointClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) AUTH_ENDPOINT;
        @NotNull final BindingProvider userBindingProvider = (BindingProvider) USER_ENDPOINT;
        @NotNull Map<String, List<String>> headers = CastUtils.cast((Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) headers = new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        userBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        initCountUser = USER_ENDPOINT.count();
    }

    @AfterClass
    public static void logout() {
        AUTH_ENDPOINT.logout();
    }

    @Before
    public void before() throws Exception {
        USER_ENDPOINT.save(user1);
    }

    @After
    public void after() throws Exception {
        USER_ENDPOINT.deleteById(user1.getId());
    }

    @Test
    public void countTest() throws Exception {
        Assert.assertEquals(initCountUser + 1, USER_ENDPOINT.count());
    }

    @Test
    public void deleteTest() throws Exception {
        Assert.assertNotNull(USER_ENDPOINT.findById(user1.getId()));
        USER_ENDPOINT.delete(user1);
        Assert.assertNull(USER_ENDPOINT.findById(user1.getId()));
        USER_ENDPOINT.save(user1);
    }

    @Test
    public void deleteByIdTest() throws Exception {
        Assert.assertNotNull(USER_ENDPOINT.findById(user1.getId()));
        USER_ENDPOINT.deleteById(user1.getId());
        Assert.assertNull(USER_ENDPOINT.findById(user1.getId()));
        USER_ENDPOINT.save(user1);
    }

    @Test
    public void findAll() throws Exception {
        Collection<UserDTO> users = USER_ENDPOINT.findAll();
        Assert.assertTrue(users.contains(user1));
    }

    @Test
    public void findByIdTest() throws Exception {
        Assert.assertEquals(USER_ENDPOINT.findById(user1.getId()), user1);
    }

    @Test
    public void saveTest() throws Exception {
        Assert.assertNull(USER_ENDPOINT.findById(user2.getId()));
        USER_ENDPOINT.save(user2);
        Assert.assertNotNull(USER_ENDPOINT.findById(user2.getId()));
        USER_ENDPOINT.deleteById(user2.getId());
    }

}