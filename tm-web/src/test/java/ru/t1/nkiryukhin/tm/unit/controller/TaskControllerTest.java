package ru.t1.nkiryukhin.tm.unit.controller;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.nkiryukhin.tm.config.AuthenticationEntryPoint;
import ru.t1.nkiryukhin.tm.config.WebApplicationConfiguration;
import ru.t1.nkiryukhin.tm.dto.TaskDTO;
import ru.t1.nkiryukhin.tm.repository.TaskDTORepository;
import ru.t1.nkiryukhin.tm.util.UserUtil;

import java.util.Collection;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
public class TaskControllerTest {

    @NotNull
    private final TaskDTO task1 = new TaskDTO("Test Unit Task 1");

    @NotNull
    private final TaskDTO task2 = new TaskDTO("Test Unit Task 2");

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private TaskDTORepository taskRepository;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @Nullable
    private String userId;

    @Before
    public void before() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        task1.setUserId(userId);
        task2.setUserId(userId);
        taskRepository.save(task1);
        taskRepository.save(task2);
    }

    @After
    @SneakyThrows
    public void after() {
        taskRepository.deleteAllByUserId(userId);
    }

    @Test
    @SneakyThrows
    public void createTest() {
        @NotNull final String url = "/task/create";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        @NotNull final Collection<TaskDTO> tasks = taskRepository.findAllByUserId(userId);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(3, tasks.size());
    }

    @Test
    @SneakyThrows
    public void deleteTest() {
        @NotNull final String url = "/task/delete/" + task1.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        @Nullable final TaskDTO task = taskRepository.findByUserIdAndId(userId, task1.getId()).orElse(null);
        Assert.assertNull(task);
    }

    @Test
    @SneakyThrows
    public void editTest() {
        @NotNull final String url = "/task/edit/" + task1.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void findAllTest() {
        @NotNull final String url = "/tasks";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

}
