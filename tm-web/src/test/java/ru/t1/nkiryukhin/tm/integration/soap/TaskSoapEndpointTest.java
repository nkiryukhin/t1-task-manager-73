package ru.t1.nkiryukhin.tm.integration.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.data.util.CastUtils;
import org.springframework.http.HttpHeaders;
import ru.t1.nkiryukhin.tm.api.AuthEndpoint;
import ru.t1.nkiryukhin.tm.api.TaskEndpoint;
import ru.t1.nkiryukhin.tm.client.soap.AuthSoapEndpointClient;
import ru.t1.nkiryukhin.tm.client.soap.TaskSoapEndpointClient;
import ru.t1.nkiryukhin.tm.dto.TaskDTO;
import ru.t1.nkiryukhin.tm.marker.IntegrationCategory;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.net.MalformedURLException;
import java.util.*;

@Category(IntegrationCategory.class)
public class TaskSoapEndpointTest {

    @NotNull
    private static AuthEndpoint AUTH_ENDPOINT;

    @NotNull
    private static TaskEndpoint TASK_ENDPOINT;

    @NotNull
    private static final String URL = "http://localhost:8080";

    @NotNull
    private final TaskDTO task1 = new TaskDTO("Test Task 1");

    @NotNull
    private final TaskDTO task2 = new TaskDTO("Test Task 2");

    @NotNull
    private final TaskDTO task3 = new TaskDTO("Test Task 3");

    @Nullable
    private static String USER_ID;

    @BeforeClass
    public static void beforeClass() throws MalformedURLException {
        AUTH_ENDPOINT = AuthSoapEndpointClient.getInstance(URL);
        Assert.assertTrue(AUTH_ENDPOINT.login("test", "test").getSuccess());
        TASK_ENDPOINT = TaskSoapEndpointClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) AUTH_ENDPOINT;
        @NotNull final BindingProvider projectBindingProvider = (BindingProvider) TASK_ENDPOINT;
        @NotNull Map<String, List<String>> headers = CastUtils.cast((Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) headers = new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        projectBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        USER_ID = AUTH_ENDPOINT.profile().getId();
    }

    @AfterClass
    public static void logout() {
        AUTH_ENDPOINT.logout();
    }

    @Before
    public void before() throws Exception {
        task1.setUserId(USER_ID);
        task2.setUserId(USER_ID);
        TASK_ENDPOINT.save(task1);
        TASK_ENDPOINT.save(task2);
    }

    @After
    public void after() throws Exception {
        TASK_ENDPOINT.clear();
    }

    @Test
    public void countTest() throws Exception {
        Assert.assertEquals(2, TASK_ENDPOINT.count());
    }

    @Test
    public void clearTest() throws Exception {
        TASK_ENDPOINT.clear();
        Assert.assertNull(TASK_ENDPOINT.findById(task1.getId()));
        Assert.assertNull(TASK_ENDPOINT.findById(task2.getId()));
    }

    @Test
    public void deleteTest() throws Exception {
        Assert.assertNotNull(TASK_ENDPOINT.findById(task1.getId()));
        TASK_ENDPOINT.delete(task1);
        Assert.assertNull(TASK_ENDPOINT.findById(task1.getId()));
    }

    @Test
    public void deleteByIdTest() throws Exception {
        Assert.assertNotNull(TASK_ENDPOINT.findById(task1.getId()));
        TASK_ENDPOINT.deleteById(task1.getId());
        Assert.assertNull(TASK_ENDPOINT.findById(task1.getId()));
    }

    @Test
    public void deleteListTest() throws Exception {
        List<TaskDTO> projects = (List<TaskDTO>) TASK_ENDPOINT.findAll();
        Assert.assertTrue(projects.contains(task1));
        Assert.assertTrue(projects.contains(task2));
        TASK_ENDPOINT.deleteList(projects);
        projects = (List<TaskDTO>) TASK_ENDPOINT.findAll();
        Assert.assertNull(projects);
    }

    @Test
    public void findAll() throws Exception {
        Collection<TaskDTO> projects = TASK_ENDPOINT.findAll();
        Assert.assertTrue(projects.contains(task1));
        Assert.assertTrue(projects.contains(task2));
    }

    @Test
    public void findByIdTest() throws Exception {
        Assert.assertEquals(TASK_ENDPOINT.findById(task1.getId()), task1);
    }

    @Test
    public void saveTest() throws Exception {
        Assert.assertNull(TASK_ENDPOINT.findById(task3.getId()));
        TASK_ENDPOINT.save(task3);
        Assert.assertNotNull(TASK_ENDPOINT.findById(task3.getId()));
    }

}