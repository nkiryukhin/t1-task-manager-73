package ru.t1.nkiryukhin.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.nkiryukhin.tm.config.AuthenticationEntryPoint;
import ru.t1.nkiryukhin.tm.config.WebApplicationConfiguration;
import ru.t1.nkiryukhin.tm.dto.ProjectDTO;
import ru.t1.nkiryukhin.tm.enumerated.Status;
import ru.t1.nkiryukhin.tm.marker.UnitCategory;
import ru.t1.nkiryukhin.tm.util.UserUtil;

import java.nio.file.AccessDeniedException;
import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
public class ProjectEndpointTest {

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    private static final String PROJECTS_URL = "http://localhost:8080/api/projects/";

    @NotNull
    private final ProjectDTO project1 = new ProjectDTO("Test Unit Project 1");

    @NotNull
    private final ProjectDTO project2 = new ProjectDTO("Test Unit Project 2");

    @NotNull
    private final ProjectDTO project3 = new ProjectDTO("Test Unit Project 3");

    @Before
    public void initTest() throws AccessDeniedException {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        project1.setUserId(UserUtil.getUserId());
        project2.setUserId(UserUtil.getUserId());
        add(project1);
        add(project2);
    }

    @After
    @SneakyThrows
    public void after() throws Exception {
        @NotNull final String url = PROJECTS_URL + "clear";
        mockMvc.perform(MockMvcRequestBuilders.delete(url).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void add(@NotNull final ProjectDTO project) {
        @NotNull final String url = PROJECTS_URL + "save";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(project);
        mockMvc.perform(MockMvcRequestBuilders.put(url).content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Nullable
    @SneakyThrows
    private ProjectDTO findById(@NotNull final String id) {
        @NotNull final String url = PROJECTS_URL + "findById/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if ("".equals(json)) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, ProjectDTO.class);
    }

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        add(project3);
        @Nullable final ProjectDTO project = findById(project3.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project3.getId(), project.getId());
    }

    @Test
    @Category(UnitCategory.class)
    public void clearTest() throws Exception {
        @NotNull final String url = PROJECTS_URL + "clear";
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(findById(project1.getId()));
        Assert.assertNull(findById(project2.getId()));
    }

    @Test
    public void countTest() throws Exception {
        @NotNull final String url = PROJECTS_URL + "count";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        Assert.assertEquals(2, objectMapper.readValue(json, Integer.class).intValue());
    }

    @Test
    public void deleteTest() throws Exception {
        @NotNull final String url = PROJECTS_URL + "delete";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(project1);
        mockMvc.perform(MockMvcRequestBuilders.delete(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(findById(project1.getId()));
    }

    @Test
    public void deleteByIdTest() throws Exception {
        @NotNull final String url = PROJECTS_URL + "deleteById/" + project1.getId();
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(findById(project1.getId()));
    }

    @Test
    public void existsByIdTest() throws Exception {
        @NotNull final String url = PROJECTS_URL + "existsById/" + project1.getId();
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        Assert.assertEquals("true", objectMapper.readValue(json, String.class));
    }

    @Test
    public void findAllTest() throws Exception {
        @NotNull final String url = PROJECTS_URL + "findAll";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final List<ProjectDTO> projects = Arrays.asList(objectMapper.readValue(json, ProjectDTO[].class));
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
    }

    @Test
    public void findByIdTest() {
        Assert.assertNotNull(findById(project1.getId()));
    }

    @Test
    public void saveTest() throws Exception {
        project1.setStatus(Status.IN_PROGRESS);
        @NotNull final String url = PROJECTS_URL + "save";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(project1);
        mockMvc.perform(MockMvcRequestBuilders.put(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        @Nullable final ProjectDTO project = findById(project1.getId());
        Assert.assertEquals(project.getStatus(), Status.IN_PROGRESS);
    }

}