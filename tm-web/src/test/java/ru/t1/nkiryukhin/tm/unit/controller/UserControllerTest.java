package ru.t1.nkiryukhin.tm.unit.controller;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.nkiryukhin.tm.config.AuthenticationEntryPoint;
import ru.t1.nkiryukhin.tm.config.WebApplicationConfiguration;
import ru.t1.nkiryukhin.tm.dto.UserDTO;
import ru.t1.nkiryukhin.tm.repository.UserDTORepository;

import java.util.Collection;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class, AuthenticationEntryPoint.class})
public class UserControllerTest {

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private UserDTORepository userRepository;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    private long initUsersCount = 0;

    private final UserDTO user1 = new UserDTO("Test Unit 1");

    @Before
    public void before() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("admin", "admin");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        initUsersCount = userRepository.count();
    }

    @Test
    @SneakyThrows
    public void createTest() {
        @NotNull String url = "/user/create";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        @NotNull Collection<UserDTO> users = userRepository.findAll();
        Assert.assertNotNull(users);
        Assert.assertEquals(initUsersCount + 1, users.size());
        @NotNull final List<UserDTO> newUsers = userRepository.findAllByLoginContainingIgnoreCase("New User");
        for (UserDTO user : newUsers) {
            userRepository.deleteById(user.getId());
        }
        users = userRepository.findAll();
        Assert.assertEquals(initUsersCount, users.size());
    }

    @Test
    @SneakyThrows
    public void deleteTest() {
        userRepository.save(user1);
        @Nullable UserDTO userDTO = userRepository.findById(user1.getId()).orElse(null);
        Assert.assertNotNull(userDTO);
        @NotNull final String url = "/user/delete/" + user1.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        userDTO = userRepository.findById(user1.getId()).orElse(null);
        Assert.assertNull(userDTO);
    }

    @Test
    @SneakyThrows
    public void editTest() {
        @NotNull final String url = "/user/edit/" + user1.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void findAllTest() {
        @NotNull final String url = "/users";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

}