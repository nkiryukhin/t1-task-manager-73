package ru.t1.nkiryukhin.tm.unit.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.nkiryukhin.tm.dto.UserDTO;
import ru.t1.nkiryukhin.tm.marker.UnitCategory;
import ru.t1.nkiryukhin.tm.repository.UserDTORepository;


@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = {DatabaseConfiguration.class})
@Category(UnitCategory.class)
public class UserRepositoryTest {

    @NotNull
    private final UserDTO user1 = new UserDTO();

    @NotNull
    @Autowired
    private UserDTORepository userRepository;

    @Before
    public void initTest() {
        user1.setLogin("test user");
        userRepository.save(user1);
    }

    @After
    public void clean() {
        userRepository.deleteById(user1.getId());
    }

    @Test
    @SneakyThrows
    public void findByLoginTest() {
        Assert.assertEquals(user1.getId(), userRepository.findByLogin(user1.getLogin()).getId());
    }

}