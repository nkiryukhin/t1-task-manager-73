package ru.t1.nkiryukhin.tm.unit.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.nkiryukhin.tm.dto.TaskDTO;
import ru.t1.nkiryukhin.tm.marker.UnitCategory;
import ru.t1.nkiryukhin.tm.repository.TaskDTORepository;
import ru.t1.nkiryukhin.tm.util.UserUtil;

import java.nio.file.AccessDeniedException;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@Category(UnitCategory.class)
public class TaskRepositoryTest {

    @NotNull
    private final TaskDTO task1 = new TaskDTO("Test Unit Task 1");

    @NotNull
    private final TaskDTO task2 = new TaskDTO("Test Unit Task 2");

    @NotNull
    @Autowired
    private TaskDTORepository taskRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() throws AccessDeniedException {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        task1.setUserId(UserUtil.getUserId());
        task2.setUserId(UserUtil.getUserId());
        taskRepository.save(task1);
        taskRepository.save(task2);
    }

    @After
    public void clean() throws AccessDeniedException {
        taskRepository.deleteByUserIdAndId(UserUtil.getUserId(), task1.getId());
        taskRepository.deleteByUserIdAndId(UserUtil.getUserId(), task2.getId());
    }

    @Test
    @SneakyThrows
    public void countByUserIdTest() {
        Assert.assertEquals(2, taskRepository.countByUserId(UserUtil.getUserId()));
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void deleteAllByUserIdTest() {
        taskRepository.deleteAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, taskRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void deleteByUserIdAndIdTest() {
        taskRepository.deleteByUserIdAndId(UserUtil.getUserId(), task1.getId());
        Assert.assertNull(taskRepository.findByUserIdAndId(UserUtil.getUserId(), task1.getId()).orElse(null));
    }

    @Test
    @SneakyThrows
    public void existByUserIdAndIdTest() {
        Assert.assertFalse(taskRepository.existsByUserIdAndId("", task1.getId()));
        Assert.assertFalse(taskRepository.existsByUserIdAndId(UserUtil.getUserId(), ""));
        Assert.assertTrue(taskRepository.existsByUserIdAndId(UserUtil.getUserId(), task1.getId()));
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void findAllTest() {
        Assert.assertEquals(2, taskRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void findByUserIdAndIdTest() {
        Assert.assertNotNull(taskRepository.findByUserIdAndId(UserUtil.getUserId(), task1.getId()));
    }

}