-- Database: tm

CREATE DATABASE tm
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Russian_Russia.1251'
    LC_CTYPE = 'Russian_Russia.1251'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1
    IS_TEMPLATE = False;


-- SCHEMA: public

CREATE SCHEMA IF NOT EXISTS public
    AUTHORIZATION postgres;

COMMENT ON SCHEMA public
    IS 'standard public schema';

GRANT ALL ON SCHEMA public TO PUBLIC;

GRANT ALL ON SCHEMA public TO postgres;


-- Type: role

DROP TYPE IF EXISTS public.role;

CREATE TYPE public.role AS ENUM
    ('USUAL', 'ADMIN');

ALTER TYPE public.role
    OWNER TO postgres;


-- Type: status

DROP TYPE IF EXISTS public.status;

CREATE TYPE public.status AS ENUM
    ('NOT_STARTED', 'IN_PROGRESS', 'COMPLETED');

ALTER TYPE public.status
    OWNER TO postgres;


-- Table: public.tm_project

CREATE TABLE IF NOT EXISTS public.tm_project
(
    id text COLLATE pg_catalog."default" NOT NULL,
    created timestamp without time zone,
    name text COLLATE pg_catalog."default",
    description text COLLATE pg_catalog."default",
    user_id text COLLATE pg_catalog."default",
    status status,
    CONSTRAINT tm_project_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tm_project
    OWNER to postgres;


-- Table: public.tm_session

CREATE TABLE IF NOT EXISTS public.tm_session
(
    id text COLLATE pg_catalog."default" NOT NULL,
    created timestamp without time zone,
    user_id text COLLATE pg_catalog."default",
    role role,
    CONSTRAINT tm_session_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tm_session
    OWNER to postgres;


-- Table: public.tm_task

CREATE TABLE IF NOT EXISTS public.tm_task
(
    id text COLLATE pg_catalog."default" NOT NULL,
    created timestamp without time zone,
    name text COLLATE pg_catalog."default",
    description text COLLATE pg_catalog."default",
    user_id text COLLATE pg_catalog."default",
    status status,
    project_id text COLLATE pg_catalog."default",
    CONSTRAINT tm_task_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tm_task
    OWNER to postgres;


-- Table: public.tm_user

CREATE TABLE IF NOT EXISTS public.tm_user
(
    id text COLLATE pg_catalog."default" NOT NULL,
    login text COLLATE pg_catalog."default",
    password text COLLATE pg_catalog."default",
    email text COLLATE pg_catalog."default",
    locked boolean,
    first_name text COLLATE pg_catalog."default",
    last_name text COLLATE pg_catalog."default",
    middle_name text COLLATE pg_catalog."default",
    role role,
    CONSTRAINT tm_user_pkey PRIMARY KEY (id),
    CONSTRAINT login_unique UNIQUE (login)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tm_user
    OWNER to postgres;